"""
題目，返回循環起始的節點
注意，不能修改 linklist，且不能增加額外的記憶體空間

TestCase，
    input: [3,2,0,-4], pos = 1 代表將尾節點的 next 指向第 1 個節點(節點值為2的節點)，
    output: 節點值為2的節點

    input: [1,2], pos = 0 代表將尾節點的 next 指向第 0 個節點(節點值為1的節點)，
    output: 節點值為1的節點

    input: [1], pos = -1 代表將尾節點的 next 指向第 -1 個節點(無此節點)，
    output: null

思路，
"""
# @lc app=leetcode id=142 lang=python3
# [142] Linked List Cycle II
# https://leetcode.com/problems/linked-list-cycle-ii/description/

# @lc code=start
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def detectCycle(self, head: ListNode) -> ListNode:

# @lc code=end

