# 不透過長度訊息，將字串轉換為數字
def str2num(s:str) -> int:
    value = index = 0

    while s:
        """
        loop-0: value = 0 * 10 + 5 =    0 + 5 = 5
        loop-1: value = 5 * 10 + 5 =    50 + 5 = 55
        loop-2: value = 55 * 10 + 6 =   550 + 6 = 556
        loop-3: value = 556 * 10 + 6 =  5560 + 6 = 5566

        value * 10，相當於是將位數進位
        """
        value = value * 10 + int(s[index])
        s = s [index+1:]

    return value

str2num("5566")
