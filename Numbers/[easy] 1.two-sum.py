"""
思路，把 target - 當前值 = 剩餘值，若剩餘值存在 array 中，返回剩餘值和當前值的 index
"""

# @lc app=leetcode id=1 lang=python3
# https://leetcode.com/problems/two-sum/description/
# Time O(n), Space O(n)

from typing import List

# @lc code=start
class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        result = []

        # 若只有兩個元素，則必定是此兩元素組成的和
        if len(nums) == 2: return [0,1]

        for i, value in enumerate(nums):
            # 剩餘值 = 和 - 當前值
            remain = target - value

            # 檢查剩餘值是在 nums 中
            if remain in nums[i+1:]:
                result.append(i)    # 儲存當前值的 index

                # 儲存剩餘值的 index，不找當前位置，只在其他位置中尋找
                result.append(nums.index(remain, i+1, len(nums)))
                break

        return result

    """ # ==== advanced method ====
    # 透過 {} 在每次遍歷元素時，就將當前位置的值轉換成剩餘值，
    # 並將剩餘值的位置記錄在 map 中，
    # 例如 [1,3,2], target=3, map ={剩餘值2:0, 剩餘值0:1, 剩餘值1:2 }

    def twoSum(self, num, target):
        map = {}
        for i in range(len(num)):
            if num[i] not in map:
                # 若當前值不出現在 map 中，將當前值轉換為 剩餘值後
                map[target - num[i]] = i
            else:
                return [map[num[i]], i]

        return [-1, -1] """

# @lc code=end

def deep_eq(result:List[int], expected:List[int] ) -> None:
    for i, value in enumerate(result):
        if value != expected[i]:
            assert False, f"FAIL, result={result}, expected={expected}"

def main():
    s = Solution()
    deep_eq(s.twoSum([3,2,4], 6), [1,2])
    deep_eq(s.twoSum([3,2,3], 6), [0,2])
    deep_eq(s.twoSum([3,3], 6), [0,1])
    deep_eq(s.twoSum([11, 15, 2, 7], 9), [2,3])

main()