"""
題目，重新排序 linklist 中的節點，依照(最左)(最右)(最左)(最右))(最左)的方式排序

TestCase，
    Input 1->2->3->4, output 1->4->2->3.    輸入節點為偶數
    Input 1->2->3,    output 1->3->2.       輸入節點為竒數
    Input 1,          output 1              輸入節點只有一個

思路，
"""
# @lc app=leetcode id=143 lang=python3
# [143] Reorder List
# https://leetcode.com/problems/reorder-list/description/

# @lc code=start

class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def reorderList(self, head: ListNode) -> None:
        """
        Do not return anything, modify head in-place instead.
        """

# @lc code=end

