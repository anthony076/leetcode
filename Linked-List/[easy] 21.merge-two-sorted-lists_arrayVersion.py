"""
    #   array Version，模擬 leetcode 輸入 array，會自動轉換成 LinkList
        - 不需要完整實現整個 LinkList，只需要簡單定義 LinkList，讓 LinkList 具有 head 的屬性就好

        - 在 leet code 上，會以 array 的方式串入 input，因此在 local 端測試時，
          需要手動將 array 轉換成簡單的 LinkList ( 及 LinkList 只需要包含 head 屬性，不需要實作合併方法)

		- 在實現合併方法時，由於操作 ListNode 的時候只需要 LinkList 的 headNode 來取得第一個節點，
          取得第一個節點後，所有的操作都可以直接操作 node 的 next 來完成，
          因此，操作函數的輸入參數，不需要傳入整個LinkList，只需要傳入head 即可

    #   Test case
        決定 l1 和 l2 節點值的大小，取小值並重新建立Node後，添加到l3
        # case，l1 -> None,
                l2 -> None,
        # case，l1 -> 1,
                l2 -> 2 -> 3,
        # case，l1 -> 2 -> 3,
                l2 -> 1 -> 4,
        # case，l1 -> None
                l2 -> 2 -> 3,
        # case，l1 -> 2 -> 3
                l2 -> None,
"""
# @lc app=leetcode id=21 lang=python3
# [21] Merge Two Sorted Lists
# https://leetcode.com/problems/merge-two-sorted-lists/description/

# @lc code=start
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class LinkedList:
    def __init__(self):
        self.head = None

class Solution:
    # 注意 l1/l2/l3 是 Linked-List 的頭指針，有頭指針才能定位第一個節點位置，l1 與 l2 指向第一個節點
    # 當 l1 或 l2 中的節點插入 l3 後，需要將 l1/l2 的頭指針指向新的節點
    # 當 l3 被插入新節點後，也需要將 l3 頭指針移到最後尚未插入新傑的的位置，l3 = l3.next
    def mergeTwoLists(self, l1: ListNode, l2: ListNode) -> ListNode:
        # head_ptr 用來保存最初的 head_ptr，merge 結束後，需要將指針移動到 head_ptr
        headNode = l3 = ListNode(None)

        # 每次插入後，l1 或 l2 的節點會往前移動一次，最終會指向 None
        while (l1 != None) & (l2 != None) :
            # ==== 插入 l1 的狀況 ===
            # 狀況，l2.data >= l1.data
            # 狀況，l2 的節點值為None
            if l1.val <= l2.val:
                l3.next = ListNode(l1.val)        # 建立新節點，並將被選擇的(l1)結點插入 l3 後方
                l1 = l1.next                        # l1.next，下一個 l1 節點，將l1 頭指針指向下一個 l1 的節點
            else:
                # ==== 插入 l2 的狀況 ===
                l3.next = ListNode(l2.val)        # 建立新節點，並將被選擇的(l2)結點插入 l3 後方
                l2 = l2.next                        # 移動 l1 的指針到下一個 l1 的節點

            # 將 l3 的移動指針，指向新插入的節點(新插入的在最後一個節點)
            # 下一次插入才能插到最後
            l3 = l3.next

        if l1 != None:
            l3.next = ListNode(l1.val)

        if l2 != None:
            l3.next = ListNode(l2.val)

        return headNode.next

    def show(self, headNode):
        result = []

        while headNode:
            result.append(headNode.val)
            headNode = headNode.next

        print(result)

# @lc code=end

if __name__ == "__main__":
    l1 = [1,2,4]
    l2 = [1,3,4]

    ll1 = LinkedList()
    ll2 = LinkedList()

    ll1.head = ListNode(l1[0])
    ll2.head = ListNode(l2[0])

    # ==== 將 number-array 轉換為 LinkList ====
    # 注意，有此步驟才能將 array 中的元素串聯
    curNode = ll1.head
    for value in l1[1:]:
        curNode.next = ListNode(value)
        curNode = curNode.next

    curNode = ll2.head
    for value in l2[1:]:
        curNode.next = ListNode(value)
        curNode = curNode.next

    s = Solution()
    l3_head = s.mergeTwoLists(ll1.head, ll2.head)
    s.show(l3_head)
