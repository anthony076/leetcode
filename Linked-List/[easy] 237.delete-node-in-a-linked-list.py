"""
沒有頭節點的節點刪除法
思路，
    一般有頭節點(第一節點)的刪除方式，是透過頭節點找到目標節點後，再進行刪除
    此方式需要透過節點.next 的遍歷來找到目標節點
    此題目`不提供頭節點(第一節點)`，只提供要被刪除的節點，利用現有的節點來進行刪除

    1->2->3->4->5，提供 node=4
    1->2->3->5->5，將下一個節點值複製到當前節點
    1->2->3->5，   然後將下一個節點刪除 (將當前節點指向下下個節點)

TestCase:
    input, [1,2,3,4,5] node=3, output [1,2,4,5]
"""
# @lc app=leetcode id=237 lang=python3
# [237] Delete Node in a Linked List
# https://leetcode.com/problems/delete-node-in-a-linked-list/description/

# @lc code=start
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def deleteNode(self, node):
        """
        :type node: ListNode
        :rtype: void Do not return anything, modify node in-place instead.
        """
        node.val = node.next.val
        node.next = node.next.next

# @lc code=end

