"""
題目，將 linklist 反向
TestCase，
    input: [1,2,3,4,5], output:[5,4,3,2,1]
思路，
    利用迴圈將節點值取出後，將值存入 stack 中，再反向取出後，插入新的 linkedlinst 中

"""
# @lc app=leetcode id=206 lang=python3
# [206] Reverse Linked List
# https://leetcode.com/problems/reverse-linked-list/description/

# @lc code=start
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def reverseList(self, head: ListNode) -> ListNode:
        values = []

        headNode = curNode = ListNode(None)

        while head:
            values.append(head.val)
            head = head.next

        while values:
            curNode.next = ListNode(values.pop())
            curNode = curNode.next

        return headNode.next

# @lc code=end

