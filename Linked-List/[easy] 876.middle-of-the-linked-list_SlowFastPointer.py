"""
思路，
    利用快慢指針來取得中點，快指針的步長是慢指針的兩倍(slow=slow.next, fast=fast.next.next)，
    當快指針超過節點範圍時，慢指針必定在中間節點的位置

    假如鍊表長度是奇數: 1--->2--->3--->4--->5, 從1 開始遍歷5/2 = 2 （取整）次，到達3，3確實是中間結點
    1->2->3->4->5
    s  s  s
    f     f     f

    假如鍊表長度是偶數: 1--->2--->3--->4--->5--->6, 從1 開始遍歷6/2 = 3次，到達4，4 確實是中間結點的第二個結點
    1->2->3->4
    s  s
    f     f

TestCase:
    [1,2,3,4,5,6] -> even，return Node4 (index=3)
    [1,2,3,4,5] -> odd，return Node3 (index=2)
"""
# @lc app=leetcode id=876 lang=python3
# [876] Middle of the Linked List
# https://leetcode.com/problems/middle-of-the-linked-list/description/

# @lc code=start
# Definition for singly-linked list.
import math

class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def middleNode(self, head: ListNode) -> ListNode:
        fast = slow = head

        # 依照 while-loop 中的代碼，fast 和 fast.next 都不能為 None
        # slow 因為會慢於 fast，所以不會成為迴圈的限制條件
        while fast and fast.next:
            # 快指針
            fast = fast.next.next
            # 慢指針
            slow = slow.next

        return slow

# @lc code=end

