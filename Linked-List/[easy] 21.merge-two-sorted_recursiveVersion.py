"""

"""
# @lc app=leetcode id=21 lang=python3
# [21] Merge Two Sorted Lists
# https://leetcode.com/problems/merge-two-sorted-lists/description/


# @lc code=start
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class LinkedList:
    def __init__(self):
        self.head = None

class Solution:
    def mergeTwoLists(self, l1: ListNode, l2: ListNode) -> ListNode:
        head_ptr = l3 = ListNode(None)

        """ while (l1 != None) & (l2 != None) :

            if l1.val <= l2.val:
                l3.next = ListNode(l1.val)
                l1 = l1.next
            else:
                l3.next = ListNode(l2.val)
                l2 = l2.next

            l3 = l3.next

        if l1 != None:
            l3.next = ListNode(l1.val)

        if l2 != None:
            l3.next = ListNode(l2.val)

        return head_ptr.next """

        if not l1 or not l2:
            return l1 or l2

        if l1.val < l2.val:
            l1.next = self.mergeTwoLists(l1.next, l2)
            return l1
        else :
            l2.next = self.mergeTwoLists(l1, l2.next)
            return l2

    def show(self, headNode):
        result = []

        while headNode:
            result.append(headNode.val)
            headNode = headNode.next

        print(result)

# @lc code=end

if __name__ == "__main__":
    l1 = [1,2,4]
    l2 = [1,3,4]

    ll1 = LinkedList()
    ll2 = LinkedList()

    ll1.head = ListNode(l1[0])
    ll2.head = ListNode(l2[0])

    # ==== 將 number-array 轉換為 LinkList ====
    # 注意，有此步驟才能將 array 中的元素串聯
    curNode = ll1.head
    for value in l1[1:]:
        curNode.next = ListNode(value)
        curNode = curNode.next

    curNode = ll2.head
    for value in l2[1:]:
        curNode.next = ListNode(value)
        curNode = curNode.next

    s = Solution()
    l3_head = s.mergeTwoLists(ll1.head, ll2.head)
    s.show(l3_head)