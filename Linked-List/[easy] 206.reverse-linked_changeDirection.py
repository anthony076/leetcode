"""
題目，將 linklist 反向
TestCase，
    input: [1,2,3,4,5], output:[5,4,3,2,1]
思路，
    將節點的 next 改變方向，轉向 -> 移動
    將 1->2->3->Null，變成，null <-1<-2<-3

    step1，取得需要的變數，保存原來的下節點
    step2，重置當前節點的next 指針方向，指向上節點
    step3，移動 preNode 和 curNode

"""
# @lc app=leetcode id=206 lang=python3
# [206] Reverse Linked List
# https://leetcode.com/problems/reverse-linked-list/description/

# @lc code=start
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def reverseList(self, head: ListNode) -> ListNode:
        preNode = None
        curNode = head

        while curNode:
            # 保存原來的下節點
            nextNode = curNode.next

            # 重置當前節點的next 指針方向，指向上節點
            curNode.next = preNode

            # 移動 preNode 和 curNode
            preNode = curNode
            curNode = nextNode

        return preNode

# @lc code=end

