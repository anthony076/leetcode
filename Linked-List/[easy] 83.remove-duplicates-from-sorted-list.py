#
# @lc app=leetcode id=83 lang=python3
# [83] Remove Duplicates from Sorted List
# https://leetcode.com/problems/remove-duplicates-from-sorted-list/description/


# @lc code=start
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class LinkedList:
    def __init__(self):
        self.head = None

class Solution:
    def deleteDuplicates(self, head: ListNode) -> ListNode:
        """
        去除下一個節點，並移動到下一個節點
        curNode.next = curNode.next.next
        curNode = curNode.next

        其中，curNode.next != Node

        """
        curNode = head

        while curNode:

            while curNode.next and curNode.next.val == curNode.val:
                curNode.next = curNode.next.next

            curNode = curNode.next

        return head

    def show(self, headNode):
        result = []

        while headNode:
            result.append(headNode.val)
            headNode = headNode.next

        print(result)

# @lc code=end

if __name__ == "__main__":
    l1 = [1, 1, 2, 3, 3, 4, 8, 8]

    ll1 = LinkedList()
    ll1.head = ListNode(l1[0])

    # ==== 將 number-array 轉換為 LinkList ====
    # 注意，有此步驟才能將 array 中的元素串聯
    curNode = ll1.head

    for value in l1[1:]:
        curNode.next = ListNode(value)
        curNode = curNode.next

    s = Solution()
    head = s.deleteDuplicates(ll1.head)
    s.show(head)

