"""
題目，不可利用反向 linklist 實現的加法

TestCase，
    input，(7 -> 2 -> 4 -> 3) + (5 -> 6 -> 4)
    output，7 -> 8 -> 0 -> 7

思路，
    - 利用 反向linklist，可以依序先計算低位數的和，並將進位帶到高位數中
      但此題目中，不可利用反向linklist，必須先計算高位數
    - 利用遍歷的方式，分別取得 l1/l2 的數字，
    進行運算後，再將數字轉換為 linklist
"""
# @lc app=leetcode id=445 lang=python3
# [445] Add Two Numbers II
# https://leetcode.com/problems/add-two-numbers-ii/description/
# @lc code=start

class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:
        if (not l1) and (not l2):
            return None

        l1_value = 0
        l2_value = 0
        while l1 or l2:
            if l1:
                l1_value = l1_value * 10 + l1.val
                l1 = l1.next
            if l2:
                l2_value = l2_value * 10 + l2.val
                l2 = l2.next

        sum_str = str(l1_value + l2_value)

        rootNode = curNode = ListNode(0)
        for num in sum_str:
            curNode.next = ListNode(int(num))
            curNode = curNode.next

        return rootNode.next

# @lc code=end

