"""
題目，刪除具有指定值的所有節點

TestCase，
    [1,2,3,6,4,5,6] -> [1,2,3,4,5]
    [6,6,1] -> [1]  指定值出現在頭部
    [1,6,6] -> [1]  指定值出現在尾部

思路，
    錯誤方法，
    檢查下一個 node 的值 ( curNode.next.val )是否是指定刪除值， => 缺點1，檢查不到第一個節點
    若是，將 curNode.next = curNode.next.next                 => 缺點2，最後一個節點沒有 curNode.next.next

    正確方法，
    改善缺點1，在第一個節點前插入 FakeNode，讓第一節點變成第二節點
    改善缺點2，當遇到刪除節點時，只做刪除，不做移動
"""
# @lc app=leetcode id=203 lang=python3
# [203] Remove Linked List Elements
# https://leetcode.com/problems/remove-linked-list-elements/description/
#
# @lc code=start
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def removeElements(self, head: ListNode, val: int) -> ListNode:
        fakeHead = ListNode(0)
        fakeHead.next = head
        curNode = fakeHead

        while curNode.next != None:
            if curNode.next.val == val:
                # curNode.next.next 有可能會是 None，
                # 造成 curNode.next = None
                curNode.next = curNode.next.next

                # 如果 curNode.next = None, 此行會造成 curNode = None
                # 造成下一個 loop 時，curNode.next = None.next 引發錯誤
                # 所以不需要加入 curNode = curNode.next
                # 下一次 loop 時，因為 curNode.next 已經重新定向，
                # 因此，curNode 仍然會在 else 中，繼續往前移動
                #curNode = curNode.next
            else:
                curNode = curNode.next

        return fakeHead.next

# @lc code=end

