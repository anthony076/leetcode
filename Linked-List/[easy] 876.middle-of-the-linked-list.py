"""
TestCase:
    [1,2,3,4,5,6] -> even，return Node4 (index=3)
    [1,2,3,4,5] -> odd，return Node3 (index=2)
"""
# @lc app=leetcode id=876 lang=python3
# [876] Middle of the Linked List
# https://leetcode.com/problems/middle-of-the-linked-list/description/

# @lc code=start
# Definition for singly-linked list.
import math

class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def middleNode(self, head: ListNode) -> ListNode:
        len = self.findLength(head)

        # 判斷奇數或偶數，取得正確的 middle-index
        if len % 2 == 0:
            imiddle = math.ceil(len/2)
        else:
            imiddle = math.ceil(len/2)-1

        index = 0
        curNode = head
        while index < imiddle:
            curNode = curNode.next
            index += 1

        return curNode

    def findLength(self, head:ListNode) -> int:
        curNode = head
        length = 0
        while curNode:
            length += 1
            curNode = curNode.next
        return length


# @lc code=end

