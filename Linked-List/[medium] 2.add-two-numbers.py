"""
題目，利用反向 LinkList 執行正整數的加法運算
TestCase，
    Input, [2,4,3] [5,6,4] output, [7,0,8]，342+465=807         ，進位在中間
    Input, [0,0,9] [0,0,1] output, [0,0,0,1]，900+100 = 1000    ，進位在末端
    Input, [5] [5]         output, [0,1]]，5+5=10               ，進位在頭部
思路，
    - 要取得每次加法的進位 (carry = sum / 10) 和 扣除進位之後的結果 (result= sum % 10)

    - 當位數不等長的時候( 例如，123+45)，缺少的位數要補 0
      l1 有值 但 l2 為 None 的處理，若使用 l1.val + l2.val + carry 會報錯
      因此不要使用 l1.val/l2.val 進行加法的運算，必須提前處理

    - 最高位的進位也要列入計算 => while l1 or l2 or carry
"""

# @lc app=leetcode id=2 lang=python3
# [2] Add Two Numbers
# https://leetcode.com/problems/add-two-numbers/description/
# @lc code=start

class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:

        carry = 0

        # curNode 是工作指針，rooNode 用來儲存頭節點，用來返回最後的結果
        rootNode = curNode = ListNode(None)

        while l1 or l2 or (carry != 0 ):
            v1 = v2 = 0

            if l1 :
                v1 = l1.val
                l1 = l1.next

            if l2 :
                v2 = l2.val
                l2 = l2.next

            # ==== 計算當前位元的和 ====
            # 錯誤寫法，l1/l2 為 None 的時候，l1.val/l2.val 無效
            #sum = l1.val + l2.val + carry   # carry 為前一位的進位值

            # 正確寫法
            sumValue = v1 + v2 + carry

            # ===== 計算 1_新的進位值 和 2_當前位元顯示的值 ====
            carry = int(sumValue / 10)
            result = sumValue % 10

            # ==== 將結果插入到新節點上 ====
            curNode.next = ListNode(result)
            curNode = curNode.next

            # 錯誤寫法，l1，和 l2 可能為 None
            #l1 = l1.next
            #l2 = l2.next

        return rootNode.next

# @lc code=end

