
"""
思路，
    - 因為是 inline solution，因此不需要建立新的 LinkList，直接修改原有的 LinkList

    - 因為重複有可能發生在頭部，因此需要一個額外的 fakeNode 來當作第一個 node，
      避免重複發生在頭部時，沒有 node 指向正確的 node，
      例如，1->1->2 時，fakeNode->2，返回時，返回 fakeNode.next

    - fakeNode，用來返回最後的結果
      preNode，用來儲存不重複的前節點
      curNode，移動節點(工作節點)
      發現重複值前，透過 preNode 用來記錄不重複的節點，
      發現重複值後，透過 curNode 來移動，並且不列入 preNode 中，最後透過 fakeNode 返回最後結果

    - Test Case
    Input:[1,1,1,2,3]           Output:[2,3]    重複在頭部
    Input:[1,2,3,3,4,4,5]       Output:[1,2,5]  重複在中間
    Input:[1,2,3,3]             Output:[1,2]    重複在尾部
    Input:[1,1,2,3,3,4,5,5]     Output:[2,4]    重複在頭、中、尾
"""
# @lc app=leetcode id=82 lang=python3
# [82] Remove Duplicates from Sorted List II
# https://leetcode.com/problems/remove-duplicates-from-sorted-list-ii/description/

# @lc code=start
# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class LinkList:
    def __init__(self):
        self.head = None

class Solution:
    def deleteDuplicates(self, head: ListNode) -> ListNode:
        fakeNode = preNode = ListNode(None)
        fakeNode.next = head
        curNode = head

        while curNode:
            if curNode.next and (curNode.val == curNode.next.val):
                # ==== 當前節點值與下一節點值相等 ====
                # 紀錄重複節點值
                preValue = curNode.val
                # 紀錄後移動到下一個節點
                curNode = curNode.next

                # 將 curNode 移動到最後一個重複節點值的節點
                while curNode and curNode.val == preValue:
                    curNode = curNode.next

                # 移動 curNode 節點後，將 preNode.next 指向 None，
                # 因為 preNode 指向不重複的前節點，因此在重複區的節點都不會被列入 preNode 中，
                # 相當於是刪除重複區的節點
                preNode.next = None

            else:
                # ==== 當前節點值與下一節點值不相等 ====
                # 先將目前節點位置保存在 preNode 後，在移動 curNode
                preNode.next = curNode
                preNode = curNode
                curNode = curNode.next

        return fakeNode.next

    def show(self, headNode):
        result = []

        while headNode:
            result.append(headNode.val)
            headNode = headNode.next

        print(result)

# @lc code=end
if __name__ == "__main__":
    l1 = [1,1,1,2,3]
    ll1 = LinkList()
    ll1.head = ListNode(l1[0])

    curNode = ll1.head
    for value in l1[1:]:
        curNode.next = ListNode(value)
        curNode = curNode.next

    s = Solution()
    head = s.deleteDuplicates(ll1.head)
    s.show(head)