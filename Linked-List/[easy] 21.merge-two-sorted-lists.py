"""
# Linked-List 在進行操作的時候，需要透過頭節點來定位，
  因此，操作時需要傳入頭節點，而不是傳入整個 Linked-List
# l1/l2/l3 都是頭指針，指向第一個節點，將結點插入 l3 後，需要將頭指針指向下一個節點
  例如 l1 = l1.next

# time-O: O(l1 移動的次數 + l2 移動的次數) = O(n)
# space-O: O(l1 移動的次數 + l2 移動的次數) = O(n)

"""
# @lc app=leetcode id=21 lang=python3
# [21] Merge Two Sorted Lists
# https://leetcode.com/problems/merge-two-sorted-lists/description/

"""
#   Test Case:
    Input: 1->2->4, 1->3->4
    Output: 1->1->2->3->4->4

    Input: 1->2->4, 1->3
    Output: 1->1->2->3->4

#   Definition for singly-linked list.
    class ListNode:
        def __init__(self, x):
            self.val = x
            self.next = None
"""
# @lc code=start
class ListNode:
    def __init__(self, x=None):
        self.x = x
        self.next = None

class LinkedList:
    def __init__(self):
        self.head = None

    # 根據題目要求，需要進行尾插入
    def insert(self, data):
        newNode = ListNode(data)

        if(self.head):
            # ==== 已經有多個其他節點，新節點加在尾部 ====
            # FROM `head -> nodeA -> nodeB` TO `head -> nodeA -> nodeB --> newNode`

            # 從頭節點取得第一個節點的位置
            curNode = self.head

            # 移動到最後一個節點
            while curNode.next:
                curNode = curNode.next

            # 執行尾插入，將 curNode.next 指向新插入的節點
            curNode.next= newNode
        else:
            # ==== 沒有有其他節點 ====
            # from head -> None to head -> newNode
            self.head = newNode

    def show(self):
        """
        case1.  head -> None
        case2.  head -> NodeA -> NodeB -> Node
        """
        result = []

        if self.head:
            curNode = self.head

            while curNode.next:
                result.append(curNode.x)
                curNode = curNode.next

            result.append(curNode.x)

        print(result)

class Solution:
    # 注意 l1/l2/l3 是 Linked-List 的頭指針，有頭指針才能定位第一個節點位置，l1 與 l2 指向第一個節點
    # 當 l1 或 l2 中的節點插入 l3 後，需要將 l1/l2 的頭指針指向新的節點
    # 當 l3 被插入新節點後，也需要將 l3 頭指針移到最後尚未插入新傑的的位置，l3 = l3.next
    def mergeTwoLists(self, l1: ListNode, l2: ListNode) -> ListNode:
        # head_ptr 用來保存最初的 head_ptr，merge 結束後，需要將指針移動到 head_ptr
        head_ptr = l3 = ListNode()

        # when l1 or l2 ，當l1或l1未到達None
        # 每次插入後，該節點會往前移動一次，最終會指向 None
        while l1 or l2:
            # 決定 l1 和 l2 節點值的大小，取小值並重新建立Node後，添加到l3
            # case，l1 -> None,     while-loop stop
            #       l2 -> None,
            # case，l1 -> 1,
            #       l2 -> 2 -> 3,   放入 l1 ，l2.data >= l1.data，次要條件
            # case，l1 -> 2 -> 3,
            #       l2 -> 1 -> 4,   放入 l2 ，l1.data >= l2.data，次要條件
            # case，l1 -> None
            #       l2 -> 2 -> 3,   l1 提前到達終點
            # case，l1 -> 2 -> 3
            #       l2 -> None,     l2 提前到達終點

            # ==== 插入 l1 的狀況 ===
            # 狀況一，l1 和 l2 都不是 None (必要條件，用 and)
            # 狀況二，l2.data >= l1.data (非必要條件，用 or)
            # 狀況三，l2 的節點值為None (非必要條件，用 or)
            if l1 and (not l2 or l1.x <= l2.x):
                l3.next = ListNode(l1.x)        # 建立新節點，並將被選擇的(l1)結點插入 l3 後方
                l1 = l1.next                        # l1.next，下一個 l1 節點，將l1 頭指針指向下一個 l1 的節點

            # ==== 插入 l2 的狀況 ===
            else:
                l3.next = ListNode(l2.x)        # 建立新節點，並將被選擇的(l2)結點插入 l3 後方
                l2 = l2.next                        # 移動 l1 的指針到下一個 l1 的節點

            # 將工作指向指向最後一個節點
            l3 = l3.next

        return head_ptr.next


# @lc code=end

if __name__ == "__main__":
    l1 = LinkedList()
    l1.insert(1)
    l1.insert(2)
    l1.insert(4)
    #l1.show()

    l2 = LinkedList()
    l2.insert(1)
    l2.insert(3)
    l2.insert(4)
    #l2.show()

    s = Solution()
    l3 = LinkedList()
    l3.head = s.mergeTwoLists(l1.head, l2.head)

    l3.show()
