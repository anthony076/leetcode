#
# @lc app=leetcode id=13 lang=python3
# 思路
#   1(X)當輸入的字數 == 兩位數字時，套用左減右加的規則
#       當輸入的字數 > 兩位數字時，套用由左至右依序遞加的規則
#   2(O)不需要拆字串，當套用左減時，數值會按著一定的比率減小，只需要判斷是否是左減 (左小於右 或 右大於左)

# @lc code=start
class Solution:
    def romanToInt(self, s: str) -> int:
        length = len(s)
        map = {"I":1, "V":5, "X":10, "L":50, "C":100, "D":500, "M":1000}
        """
        IV(4 = 6-2 = 6-2I) VI(6)
        IX(9 = 11-2 = 11-2I) XI(11)

        XL(40 = 60-20 = 60-2X) LX(60)
        XC(90 = 110-20 = 110-2X) CX(110)

        CM(900 = 1100-200 = 1100-2C) MC(1100)
        CD(400 = 600-200 = 600-2C) DC(600)
        """
        result = 0
        for i in range(0, len(s), 1):

            #if i > 0 and s[i] < s[i+1]:    #錯誤寫法，會有超出邊界的疑慮
            if i > 0 and (map[s[i] ]> map[s[i-1]]):
                # 左側比右側小，套用左減規則，會比正常值小一定的比例
                result += map[s[i]] - 2*map[s[i-1]]
            else :
                result += map[s[i]]

        return result

# @lc code=end

def check (input:str, expected:int) -> None:
    result = s.romanToInt(input)
    assert  result == expected, f"FAIL, input={input}, expected={expected}, output={result}"

def main():
    #從 index=1 開始，
    check("MCMXCIV", 1994)

s = Solution()
main()

