# ==== Dont delete below line ====
# @lc app=leetcode id=344 lang=python3

from typing import List

# @lc code=start
class Solution:
    def reverseString(self, s: List[str]) -> None:
        """
        Do not return anything, modify s in-place instead.
        """

        i = 0
        j = len(s)-1

        while i<j:
            s[i], s[j] = s[j], s[i]
            i+=1
            j-=1
# @lc code=end

def deep_eq (inputStr:List[str], expected:List[str] ) -> None:
    for i, inputValue in enumerate(inputStr):
        if inputValue != expected[i]:
            assert False, f"FAIL, inputStr={inputValue}, expected={expected}"

inputStr = ["h","e","l","l","o"]
expected = ["o","l","l","e","h"]

s = Solution()
s.reverseString(inputStr)

deep_eq(inputStr, expected)
