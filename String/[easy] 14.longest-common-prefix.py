"""
思路1，利用指針，遍歷所有元素的相同位置，若有元素的字元不同，就停止遍歷
步驟    1. 外圈遍歷指針，內圈遍歷 array 中的元素
        2. 遍歷元素前先儲存當前的字元，若停止遍歷，再把當前字元刪除

思路2，利用 zip() 的行列轉換，取出每個位置的值，再利用 set() 判斷是否所有元素都具有相同值
"""

# @lc app=leetcode id=14 lang=python3
# [14] Longest Common Prefix
# https://leetcode.com/problems/longest-common-prefix/description/

from typing import List


# @lc code=start
class Solution:

    def findMinLength(self, strs:List[str]) -> int:
        minLength = 9999

        for element in strs:
            curLength = len(element)

            if  curLength < minLength:
                minLength = curLength

        return minLength

    def longestCommonPrefix(self, strs: List[str]) -> str:
        if len(strs) == 0:
            return ""

        same = True
        common_string=""

        minStringLength = self.findMinLength(strs)

        for i in range(minStringLength):
            common_string += strs[0][i]

            for word in strs:
                if word[i] != common_string[i]:
                    common_string = common_string[:-1]
                    same = False
                    break

            if same == False :
                break

        return common_string

# @lc code=end

if __name__ == "__main__":
    s = Solution()
    result = s.longestCommonPrefix(["flower", "flow", "flight"])

    print(result)

