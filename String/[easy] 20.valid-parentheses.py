"""
思路，
    # 遇到左符號的時候，將左符號添加到 stack，
      遇到右符號時，取出 stack 的最後一個左符號 (後進先出，用 stack)，並判斷該左符號對應的右符號，是否與當前右符號相同
    # 需要利用 key:value 的方式，映射左右符號
"""
#
# @lc app=leetcode id=20 lang=python3
# [20] Valid Parentheses
# https://leetcode.com/problems/valid-parentheses/description/
#

# @lc code=start

class Solution:
    def isValid(self, s: str) -> bool:
        """
        # 無限添加 right，遇到的 left 一定要與最後的 right 相對應
        ()[]{}  True
        ({[]})  True
        ({)}    False
        (       False
        )       False
        """
        stack = []
        match = {"{":"}", "(":")", "[":"]"}

        # add imput into Queue
        for char in s :
            if char in match:
                # 將左側符號添加到 stack 中
                stack.append(char)
            else:
                # 若是右側符號，
                # 取出 stack 中最後一位，並判斷最後一位的右側符號(match["last"]) 是否與當前的右側符號相同
                try:
                    last = stack.pop()
                except:
                    return False

                if match[last] != char or len(stack) == 0:
                    return False

        # 或用 return not stack 簡化
        if len(stack) > 0:
            return False
        else:
            return True

# @lc code=end
if __name__ == "__main__":
    s = Solution()
    result = s.isValid("{")
    print(result)
