"""
題目，判斷 linklist 是否形成環結構
注意，空間複雜度必須為 O(1)

TestCase，
    input: [3,2,0,-4], pos = 1 代表將尾節點的 next 指向第 1 個節點(節點值為2的節點)，
    output: True，將尾節點的 next 指向節點值為2的節點時，會形成環

    input: [1,2], pos = 0 代表將尾節點的 next 指向第 0 個節點(節點值為1的節點)，
    output: True，將尾節點的 next 指向節點值為1的節點時，會形成環

    input: [1], pos = -1 代表將尾節點的 next 指向第 -1 個節點(無此節點)，
    output: False，將尾節點的 next 指向不存在的節點時，不會形成環
"""
# @lc app=leetcode id=141 lang=python3
# [141] Linked List Cycle
# https://leetcode.com/problems/linked-list-cycle/description/

# @lc code=start

class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def hasCycle(self, head: ListNode) -> bool:

# @lc code=end

