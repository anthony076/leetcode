"""
題目:
輸入兩個數字，驗證兩個數字產生的乘積是否是吸血鬼數

吸血鬼數，
    為輸入數字的`任意排列組合` (輸入數字中的每個數字，必然出現在乘積結果中)，
    且輸入的兩個數字，不能同時偶數

TestCase，
    15*93 = 1395        返回 True，輸入數字，"1"、"5"、"9"、"3"、都有出現在乘積 1395 中，因此 15、93 是吸血鬼數
    21*60 = 1260        返回 True
    21*87 = 1827        返回 True
    210*600 = 126000    返回 False，因為輸入的兩個數字都是偶數，因此不是吸血鬼數
"""

def vimpireNumber_string(x:int, y:int) -> bool:
    if (x%2 == 0) and (y%2 == 0) :
        return False

    product = str(x * y)
    factors = [ el for el in str(x) + str(y) ]

    for num in product:
        if not num in factors:
            return False

    return True

testCase = [
        (210,600, False),
        (15, 93, True),
        (21, 60, True),
        (21,87, True),
        (1620,8073, True),
        (1863,7020, True),
        (246, 510, False),
        (2070, 6318, False),
    ]

for (x, y, result) in testCase:
    if result == False:
        assert not vimpireNumber(x,y), f"{x}, {y}"
    else:
        assert vimpireNumber(x,y)